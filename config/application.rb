# saral_street_view/config/application.rb
$LOAD_PATH << File.join(File.dirname(__FILE__), "..", "app", "controllers")
require 'saral'
require 'courses_controller'

module SaralStreetView
  class Application < Saral::Application
  end
end
