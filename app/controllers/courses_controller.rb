# street_view/app/controllers/courses_controller.rb

class CoursesController < Saral::Controller

  def create
    "This is a new course."
  end

  def delete
    "This is the delete action"
  end

  def modify
    'This is the modify action'
  end

end
